// --------------
// RunCollatz.cpp
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, endl
#include <sstream>  // istringstream
#include <string>   // getline, string

#include "Collatz.hpp"

using namespace std;

// ----
// main
// ----

int main () {
    string s;
    while (getline(cin, s)) {
        // ----
        // read
        // ----

        istringstream iss(s);
        unsigned i, j;
        iss >> i >> j;

        // ----
        // eval
        // ----

        unsigned v = max_cycle_length(i, j);

        // -----
        // print
        // -----
        cout << i << " " << j << " " << v << endl;
    }

    return 0;
}
