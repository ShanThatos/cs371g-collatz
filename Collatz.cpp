// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert

#include "Collatz.hpp"

using namespace std;

// ----------------
// cycle_length
// ----------------
static unsigned cache[CACHE_SIZE];
unsigned cycle_length (ull n) {
    assert(n > 0);
    if (n == 1) // Base case
        return 1;
    if (n < CACHE_SIZE && cache[n] != 0) // Cached value
        return cache[n];
    ull v = n % 2 ?
            2 + cycle_length(n + (n >> 1) + 1) : // Odd Optimization
            1 + cycle_length(n >> 1);            // Even

    if (n < CACHE_SIZE) // Caching the answer
        cache[n] = v;
    return v;
}

// ----------------
// max_cycle_length
// ----------------

unsigned max_cycle_length (unsigned i, unsigned j) {
    assert(i > 0);
    assert(j > 0);

    if (i > j) { // Switching i and j if backwards
        unsigned temp = i;
        i = j;
        j = temp;
    }

    unsigned m = j / 2 + 1; // Middle cutoff optimization
    if (m > i)
        i = m;

    unsigned v = cycle_length(i), nv;
    for (ull n = i + 1; n <= j; ++n) {
        nv = cycle_length(n);
        if (nv > v) v = nv;
    }

    assert(v > 0);
    return v;
}
