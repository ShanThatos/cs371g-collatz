// -----------
// Collatz.hpp
// -----------

#ifndef Collatz_hpp
#define Collatz_hpp

typedef unsigned long long ull;

// Going to lazily store cycle length of 1-10000000
#define CACHE_SIZE 10000000

// ----------------
// cycle_length
// ----------------
unsigned cycle_length (ull n);

// ----------------
// max_cycle_length
// ----------------

/**
 * @param two positive ints
 * @return one positive int
 */
unsigned max_cycle_length (unsigned i, unsigned j);

#endif // Collatz_hpp
