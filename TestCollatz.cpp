// ---------------
// TestCollatz.cpp
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------

TEST(CollatzFixture, max_cycle_length_0) {
    ASSERT_EQ(max_cycle_length(1, 10), 20u);
}

TEST(CollatzFixture, max_cycle_length_1) {
    ASSERT_EQ(max_cycle_length(100, 200), 125u);
}

TEST(CollatzFixture, max_cycle_length_2) {
    ASSERT_EQ(max_cycle_length(201, 210), 89u);
}

TEST(CollatzFixture, max_cycle_length_3) {
    ASSERT_EQ(max_cycle_length(900, 1000), 174u);
}

TEST(CollatzFixture, max_cycle_length_4) {
    ASSERT_EQ(max_cycle_length(1, 100), 119u);
}

TEST(CollatzFixture, max_cycle_length_5) {
    ASSERT_EQ(max_cycle_length(1, 1000), 179u);
}

TEST(CollatzFixture, max_cycle_length_6) {
    ASSERT_EQ(max_cycle_length(1, 10000), 262u);
}

TEST(CollatzFixture, max_cycle_length_7) {
    ASSERT_EQ(max_cycle_length(1, 100000), 351u);
}

TEST(CollatzFixture, max_cycle_length_8) {
    ASSERT_EQ(max_cycle_length(1, 999999), 525u);
}

TEST(CollatzFixture, max_cycle_length_9) {
    ASSERT_EQ(max_cycle_length(55, 40), 113u);
}

TEST(CollatzFixture, max_cycle_length_10) {
    ASSERT_EQ(max_cycle_length(101, 96), 119u);
}

TEST(CollatzFixture, cycle_length_0) {
    ASSERT_EQ(cycle_length(2), 2u);
}

TEST(CollatzFixture, cycle_length_1) {
    ASSERT_EQ(cycle_length(1), 1u);
}

TEST(CollatzFixture, cycle_length_2) {
    ASSERT_EQ(cycle_length(502441), 333u);
}

TEST(CollatzFixture, cycle_length_3) {
    ASSERT_EQ(cycle_length(4096), 13u);
}

TEST(CollatzFixture, cycle_length_4) {
    ASSERT_EQ(cycle_length(847871), 326u);
}

TEST(CollatzFixture, cycle_length_5) {
    ASSERT_EQ(cycle_length(999999), 259u);
}

TEST(CollatzFixture, cycle_length_6) {
    ASSERT_EQ(cycle_length(77031), 351u);
}

TEST(CollatzFixture, cycle_length_7) {
    ASSERT_EQ(cycle_length(704511), 243u);
}

TEST(CollatzFixture, cycle_length_8) {
    ASSERT_EQ(cycle_length(665215), 442u);
}